//
//  WeatherInfoModel.swift
//  WeatherApp
//
//  Created by Wenda Xiao on 25/2/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import Foundation

struct Weather: Decodable {
    var cityCount: Int
    var cityWeatherInfos: [CityWeatherInfo]
    
    private enum CodingKeys: String, CodingKey {
        case cityCount = "cnt"
        case cityWeatherInfos = "list"
    }
}


struct CityWeatherInfo: Decodable {
    var cityName: String
    var mainInfo: MainCityInfo
    var weatherDescription: [WeatherDescription]
    
    private enum CodingKeys: String, CodingKey {
        case cityName = "name"
        case mainInfo = "main"
        case weatherDescription="weather"
    }
}

struct WeatherDescription: Decodable {
    var shortDescription: String
    var description: String
    
    private enum CodingKeys: String, CodingKey {
        case shortDescription = "main"
        case description
    }
}

struct MainCityInfo: Decodable {
    var temp: Double
    var temp_min: Double
    var temp_max: Double
    var humidity: Int
    var pressure: Int
}
