//
//  CityInfoTableViewCell.swift
//  WeatherApp
//
//  Created by Wenda Xiao on 25/2/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit

class CityInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var cityName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
