//
//  ViewController.swift
//  WeatherApp
//
//  Created by Wenda Xiao on 25/2/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class ViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return  .lightContent
    }
    
    @IBOutlet weak var cityWeatherTableView: UITableView!
    
    private var weatherData = [CityWeatherInfo]()
    private var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestWeatherData()
        cityWeatherTableView.backgroundView = UIImageView(image: UIImage(named: "background"))
        cityWeatherTableView.delegate = self
        cityWeatherTableView.dataSource = self
        cityWeatherTableView.register(UINib.init(nibName: "CityInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "cityInfoTableViewCell")
    }
    
    private func requestWeatherData(){
        SVProgressHUD.show(withStatus: "Retrieving Data...")
        AF.request("http://api.openweathermap.org/data/2.5/group?id=4163971,2147714,2174003&units=metric&APPID=fc9e38563c33f044f53677154880ece3").responseData { (response) in
            
            if let data = response.data {
                SVProgressHUD.dismiss()
                do {
                    let decoder = JSONDecoder()
                    let decodedData = try decoder.decode(Weather.self, from: data)
                    self.weatherData = decodedData.cityWeatherInfos
                    DispatchQueue.main.async {
                        self.cityWeatherTableView.reloadData()
                    }
                } catch let jsonError {
                    print("decode error: ", jsonError)
                }
            } else {
                SVProgressHUD.showError(withStatus: "network error")
                SVProgressHUD.dismiss(withDelay: 1)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailedWeatherInfo" {
            let nextViewController = segue.destination as! DetailedWeatherInfoPageViewController
            nextViewController.weatherData = self.weatherData
            nextViewController.entryIndex = selectedIndex
        }
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cityWeatherTableView.dequeueReusableCell(withIdentifier: "cityInfoTableViewCell", for: indexPath) as! CityInfoTableViewCell
        cell.cityName.text = weatherData[indexPath.row].cityName
        cell.temp.text = String(Int(weatherData[indexPath.row].mainInfo.temp)) + "\u{2103}"
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.performSegue(withIdentifier: "showDetailedWeatherInfo", sender: self)
    }
}
