//
//  DetailedWeatherInfoDisplayViewController.swift
//  WeatherApp
//
//  Created by Wenda Xiao on 25/2/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit

class DetailedWeatherInfoDisplayViewController: UIViewController {
    @IBOutlet weak var currentDate: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var minAndMaxTemp: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var cityName: UILabel!
    
    var cityWeatherInfo: CityWeatherInfo!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDisplayInfo()
    }
    
    private func setDisplayInfo() {
        cityName.text = cityWeatherInfo.cityName
        humidity.text = "Humidity \(cityWeatherInfo.mainInfo.humidity)%"
        minAndMaxTemp.text = "Min \(Int(cityWeatherInfo.mainInfo.temp_min))\u{2103} Max \(Int(cityWeatherInfo.mainInfo.temp_max))\u{2103}"
        temp.text = "\(Int(cityWeatherInfo.mainInfo.temp))\u{2103}"
        let date = Date.init(timeIntervalSinceNow: 0)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .full
        currentDate.text = "\(dateFormatter.string(from: date))"
        weatherDescription.text = "\(capitalizeEachWord(cityWeatherInfo.weatherDescription.first!.description))"
    }
    
    private func capitalizeEachWord(_ text: String) -> String {
        let words = text.split(separator: " ")
        var newString = ""
        for word in words {
            newString += word.prefix(1).uppercased() + word.lowercased().dropFirst() + " "
        }
        return newString
    }
}