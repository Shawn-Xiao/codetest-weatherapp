//
//  DetailedWeatherInfoPageViewController.swift
//  WeatherApp
//
//  Created by Wenda Xiao on 25/2/19.
//  Copyright © 2019 Wenda Xiao. All rights reserved.
//

import UIKit

class DetailedWeatherInfoPageViewController: UIPageViewController {
    
    let pageControl = UIPageControl()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    
    var entryIndex = 0
    var weatherData = [CityWeatherInfo]()
    lazy var viewControllerArray: [UIViewController] = {
        var controllers = [UIViewController]()
        for cityWeatherInfo in weatherData {
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailedWeatherInfoDisplayViewController") as! DetailedWeatherInfoDisplayViewController
            controller.cityWeatherInfo = cityWeatherInfo
            controllers.append(controller)
        }
        return controllers
    }()
    
    var returnButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        
        
        //add returnButton
        returnButton = UIButton(frame: CGRect(x: view.frame.width-80, y: view.frame.height+30, width: 60, height: 60))
        returnButton.alpha = 0.8
        returnButton.backgroundColor = UIColor.clear
        returnButton.setImage(UIImage(named: "back"), for: .normal)
        returnButton.addTarget(self, action: #selector(returnButtonTapped), for: .touchUpInside)
        returnButton.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
        self.view.addSubview(returnButton)
        
        
        
        //set initial viewController
        let currentViewController = viewControllerArray[viewControllerArray.index(after: (entryIndex - 1))]
        setViewControllers([currentViewController],
                           direction: .forward,
                           animated: true,
                           completion: nil)
        
        
        //set pageControl
        self.pageControl.frame = CGRect()
        self.pageControl.currentPageIndicatorTintColor = UIColor.white
        self.pageControl.pageIndicatorTintColor = UIColor.gray
        self.pageControl.numberOfPages = self.viewControllerArray.count
        self.pageControl.currentPage = entryIndex
        self.view.addSubview(self.pageControl)
        
        self.pageControl.translatesAutoresizingMaskIntoConstraints = false
        self.pageControl.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -5).isActive = true
        self.pageControl.widthAnchor.constraint(equalTo: self.view.widthAnchor, constant: -20).isActive = true
        self.pageControl.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.pageControl.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 10, options: [], animations: {
            self.returnButton.transform = CGAffineTransform.init(translationX: 0, y: -110)
        }, completion: nil)
    }
    
    
    @objc func returnButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension DetailedWeatherInfoPageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = viewControllerArray.firstIndex(of: viewController) else {
            return nil
        }
        
        if let previousIndex = viewControllerArray.index(currentIndex, offsetBy: -1, limitedBy: viewControllerArray.startIndex) {
            return viewControllerArray[previousIndex]
        } else {
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = viewControllerArray.firstIndex(of: viewController) else {
            return nil
        }
        
        if let nextIndex = viewControllerArray.index(currentIndex, offsetBy: 1, limitedBy: viewControllerArray.endIndex.advanced(by: -1)) {
            return viewControllerArray[nextIndex]
        } else {
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let viewControllers = pageViewController.viewControllers {
            if let viewControllerIndex = self.viewControllerArray.index(of: viewControllers[0]) {
                self.pageControl.currentPage = viewControllerIndex
            }
        }
    }
}
